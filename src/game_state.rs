
use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;
use wasm_bindgen::prelude::*;

// super crate is lib.rs
use super::utils::*;
use super::game_object::{GameObject, VolatileObject, GameObjectType};
use super::game_camera::CameraPosition;

#[wasm_bindgen]
pub struct GameState {
    //world: World,
    player: Rc<RefCell<GameObject>>,
    objects: Rc<Vec<Rc<RefCell<GameObject>>>>,
    camera_pos: Rc<CameraPosition>,
}

/// Implementation of GameState to retrieve game state values as Rust values
impl GameState {
    pub fn get_objects_rs(&self) -> Rc<Vec<Rc<RefCell<GameObject>>>> {
        Rc::clone(&self.objects)
    }

    pub fn get_camera_pos_rs(&self) -> Rc<CameraPosition> {
        Rc::clone(&self.camera_pos)
    }

    pub fn get_object_rs(&self, index: usize) -> RefCell<GameObject>{
        RefCell::clone(&self.objects[index])
    }

    pub fn get_player_rs(&self) -> RefCell<GameObject> {
        RefCell::clone(&self.player)
    }
}

//TODO: Fix spatial map edges to use smaller cell size
const SPATIAL_MAP_CELL_SIZE: usize = 40000;

#[wasm_bindgen]
impl GameState {
    /// GameState struct to hold all of game state (objects, collisions, etc)
    pub fn new(player_size: f64) -> Self {
        GameState {
            //world: World::new(100, 100, 20),
            player: Rc::new(RefCell::new(GameObject::new(0.0, 0.0, 0.0, player_size, GameObjectType::Player))),
            objects: Rc::new(Vec::new()),
            camera_pos: Rc::new(CameraPosition::DEFAULT_CAMERA_POS),
        }
    }

    /// Returns player object of GameState in JavaScript format
    pub fn get_player(&self) -> JsValue {
        JsValue::from_serde(&*self.player).unwrap()
    }
    
    /// Returns all objects of GameState in JavaScript format (array)
    pub fn get_objects(&self) -> JsValue {
        JsValue::from_serde(&*self.objects).unwrap()
    }

    pub fn get_object(&self, index: usize) -> JsValue {
        JsValue::from_serde(&*self.objects[index]).unwrap()
    }
    
    /// Returns camera position object (camera angle/speed) in JavaScript format
    pub fn get_camera_pos(&self) -> JsValue {
        JsValue::from_serde(&*self.camera_pos).unwrap()
    }

    /// Pushes GameObject into GameState lsit of objects
    pub fn push_object(& mut self, object: GameObject) {
        Rc::get_mut(&mut self.objects).unwrap().push(Rc::new(RefCell::new(object)));
    }

    /// Main update loop of GameState. Handles inputs (keyboard/mouse) and updates
    /// all object positions and interactions accordingly.
    /// 
    /// # Arguments
    /// 
    /// `keyboard_state` - JsValue (meant to be called by JS) Map of pressed keys.
    ///
    /// ```rust,no_test
    /// //casted to Rust object
    /// let key_map: HashMap<String, HashMap<String, bool>> = keyboard_state.into_serde().unwrap();
    /// ```
    /// 
    /// `mouse_movement` - JsValue (meant to be called by JS) Map "x" or "y" String to delta mouse movement (i32).
    ///
    /// ```rust,no_test
    /// //casted to Rust object
    /// let mouse_map: HashMap<String, i32> = mouse_movement.into_serde().unwrap();
    /// ```
    /// 
    /// `delta_time` - usize (meant to be called by JS) Change in milliseconds since last game update.
    /// Value derived from JavaScript performance.now();
    pub fn update(&mut self, keyboard_state: JsValue, mouse_movement: JsValue, delta_time: usize, absolute_time: usize) {
        let key_map: HashMap<String, HashMap<String, bool>> = keyboard_state.into_serde().unwrap();
        let mouse_map: HashMap<String, i32> = mouse_movement.into_serde().unwrap();

        self.handle_collisions();

        self.player.borrow_mut().update(&key_map, &delta_time, &absolute_time, &self.camera_pos);

        for o in Rc::get_mut(&mut self.objects).unwrap() {
            &mut o.borrow_mut().go(&delta_time);
        }

        self.update_camera(&key_map, &mouse_map, &delta_time);
        
        //WebGLRenderer::req_anim_draw();
    }

    /// Helper function. Iterates through SpatialHashMap collisions and calls GameObject's `handle_collision()` method.<br>
    /// `handle_collision(&mut self, other: &mut T) where T: Position`<br>
    /// `inherited from Collidable<T> trait`.
    fn handle_collisions(&mut self) {
        let volatiles: Rc<Vec<Rc<RefCell<VolatileObject>>>> = self.player.borrow().get_children();
        let volatile_objects: Rc<Vec<Rc<RefCell<GameObject>>>> = Rc::new(volatiles.iter().map(|e| Rc::new(e.borrow().get_game_object())).collect());
        //TODO: Refactor the volatile objects
        let all_obj: Vec<Rc<Vec<Rc<RefCell<GameObject>>>>> = vec!(Rc::clone(&self.objects), Rc::clone(&volatile_objects));
        let all_obj: Vec<Rc<RefCell<GameObject>>> = all_obj.iter().fold(vec!(), |mut v, e| {
            for o in e.iter() {
                v.push(Rc::clone(o));
            }
            v
        });
        let collisions = SpatialHashMap::insert_all(SPATIAL_MAP_CELL_SIZE, Rc::clone(&self.player), Rc::new(all_obj));
        let v: Vec<(Rc<RefCell<GameObject>>, Rc<RefCell<GameObject>>)> = collisions.collect();
        for (o1, o2) in v.into_iter() {
            &mut o1.borrow_mut().handle_collision(&mut o2.borrow_mut());
        }
    }

    /// Helper function. Takes key_map and mouse_map by reference and updates the position of the game
    /// camera accordingly.
    /// 
    /// For arguments see ```GameState update()``` method.
    fn update_camera(&mut self, key_map: &HashMap<String, HashMap<String, bool>>, mouse_map: &HashMap<String, i32>, delta_time: &usize) {
        let camera: & mut CameraPosition = Rc::get_mut(& mut self.camera_pos).unwrap();
        camera.update_keys(key_map, delta_time);
        camera.update_mouse(mouse_map, delta_time);
    }

    /// Places all objects at origin `(0, 0, 0)`.<br>
    /// Debugging method marked for removal.
    // TODO: Remove this
    pub fn reset(& mut self) {
        for object in Rc::get_mut(&mut self.objects).unwrap().iter_mut() {
            object.borrow_mut().set_pos(0.0, 0.0, 0.0);
        }
    }
}

