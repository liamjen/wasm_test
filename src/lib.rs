pub mod game_state;
pub mod game_object;
pub mod game_camera;
pub mod js_renderer;
pub mod input_state;
pub mod utils;

use js_renderer::WebGLRenderer;
use input_state::InputStateRust;
use game_state::GameState;
use game_object::{GameObject, VolatileObject, GameObjectType};
use utils::{Vector3, set_panic_hook};

#[macro_use]
extern crate serde_derive;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use std::fmt::Debug;
use std::cell::RefCell;
use std::rc::Rc;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern {
    fn alert(s: &str);
}

#[wasm_bindgen]
extern {
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

fn log_debug_val<T>(o: T) where T: Debug {
    log(&*format!("{:?}", o));
}

#[wasm_bindgen]
extern {
    fn show_text(s: &str);
}

fn request_animation_frame(closure: &Closure<FnMut()>) {
    web_sys::window().unwrap().request_animation_frame(closure.as_ref().unchecked_ref()).unwrap();
    //requestAnimationFrame(closure.as_ref().unchecked_ref());
}

//TODO: Remove this
const NUM_OBJECTS: usize = 20;
const PLAYER_SIZE: f64 = 20.0;
const OBJECT_SIZE: f64 = 5.0;

// TODO: Actual game instantiation.
/// Function called upon import of WASM module to JavaScript.
#[wasm_bindgen(start)]
pub fn start() {
    set_panic_hook();
    
    let mut game_state = GameState::new(PLAYER_SIZE);
    let inputs = InputStateRust::new();
    let webgl_renderer = WebGLRenderer::new(NUM_OBJECTS);

    webgl_renderer.initialize_player(PLAYER_SIZE);
    for i in 0..NUM_OBJECTS {
        let i = i as f64;
        game_state.push_object(GameObject::new(i, i, i, OBJECT_SIZE, GameObjectType::NPC));
        webgl_renderer.push_object(OBJECT_SIZE);
    }

    game_anim_loop(game_state, inputs, webgl_renderer).unwrap_or_else(|_|{
        log("Failed to start loop");
    });
}

//TODO: Find solution for following 3 functions (object updating)
pub fn update_object(webgl_renderer: &WebGLRenderer, object: RefCell<GameObject>, index: usize) {
    let o = object.borrow();
    webgl_renderer.set_object_position(index, o.pos);
    webgl_renderer.rotate_object(index, o.rotation);
    //webgl_renderer.set_object_text(index, text: JsValue)
}

pub fn update_volatile_object(webgl_renderer: &WebGLRenderer, object: RefCell<VolatileObject>, hash: usize) {
    let volatile_object = object.borrow();
    let game_object = volatile_object.object.borrow();
    webgl_renderer.push_volatile_object(hash, game_object.size);
    webgl_renderer.set_volatile_object_position(hash, game_object.pos);
    webgl_renderer.rotate_volatile_object(hash, game_object.rotation);
}

pub fn update_player(webgl_renderer: &WebGLRenderer, player: RefCell<GameObject>) {
    let p = player.borrow();
    webgl_renderer.set_player_position(p.pos);
    webgl_renderer.rotate_player(p.rotation);

    let children = p.get_children();
    for (i, o) in children.iter().enumerate() {
        update_volatile_object(&webgl_renderer, RefCell::clone(o), i);
    }
}

/// Main game loop.
/// Calls "recursvive" JavaScript requestAnimationFrame.
pub fn game_anim_loop(mut game_state: GameState, inputs: InputStateRust, webgl_renderer: WebGLRenderer) -> Result<(), JsValue> {
    let f = Rc::new(RefCell::new(None));
    let g = f.clone();

    {
        *g.borrow_mut() = Some(Closure::wrap(Box::new(move || {
            //Main Loop
            //TODO: Refactor loop
            let keyboard_state = inputs.get_keyboard_state();
            let mouse_state = inputs.get_mouse_state();
            let delta_time = inputs.get_delta_time();
            let absolute_time = inputs.get_absolute_time();
            game_state.update(keyboard_state, mouse_state, delta_time, absolute_time);
            inputs.reset_mouse_state();
            
            update_player(&webgl_renderer, game_state.get_player_rs());

            for i in 0..NUM_OBJECTS {
                update_object(&webgl_renderer, game_state.get_object_rs(i), i);
                //webgl_renderer.set_object_text(i, webgl_renderer.debug_position_info(game_state.get_object(i)));
            }

            webgl_renderer.update(game_state.get_camera_pos());
            
            request_animation_frame(f.borrow().as_ref().unwrap());
        }) as Box<FnMut()>));
    }

    request_animation_frame(g.borrow().as_ref().unwrap());
    Ok(())
}
