use crate::utils::{SHMDebug, PlayerMove, Collidable, Position, Shoot, Vector3, Quaternion, key_pressed, console_log_1};
use crate::game_camera::CameraPosition;

use wasm_bindgen::prelude::*;

use std::cell::RefCell;
use std::rc::Rc;
use std::collections::HashMap;
use std::vec::Vec;

//TODO: Trait inheritance for Player and GameObjects ?

/// Enum describing GameObject Type.<br>
/// Designed to be used if JavaScript Graphics need to know the type of object.<br>
/// May be marked for removal in favor of a different solution.
// TODO: Remove or find better solution
#[wasm_bindgen]
#[derive(Clone, Copy, Serialize, Debug)]
pub enum GameObjectType {
    Player,
    NPC,
}

/// Struct holding all members a GameObject is concerned with (position, movements, type, etc.).
#[wasm_bindgen]
#[derive(Clone, Serialize, Debug)]
pub struct GameObject {
    pub pos: Vector3,
    pub size: f64,
    pub accel: Vector3,
    pub velocity: Vector3,
    pub rotation: Quaternion,
    pub obj_type: GameObjectType,
    children: Rc<Vec<Rc<RefCell<VolatileObject>>>>,
    //TODO: information about position in spatial map (debugging)
    //Remove this or add it as a compiler flag
    pub collision_map_pos: Vector3,
}

#[derive(Clone, Serialize, Debug)]
pub struct VolatileObject{birth_time_millis: usize, lifetime_millis: usize, pub object: RefCell<GameObject>}

impl VolatileObject {
    pub fn get_game_object(&self) -> RefCell<GameObject> {
        RefCell::clone(&self.object)
    }
}

/// Factor to decelerate by.<br>
/// Without constant deceleration Velocity will not reach zero ever.
const DECEL_FACTOR: f64 = 0.01;
const GRAVITY_FORCE: f64 = 0.1;
const DECEL_VECTOR: Vector3 = Vector3{ x: DECEL_FACTOR, y: DECEL_FACTOR, z: DECEL_FACTOR};
const GRAVITY_VECTOR: Vector3 = Vector3{ x: 0.0, y: -GRAVITY_FORCE, z: 0.0};

// Implementations of GameObject not revealed to wasm
impl GameObject {
    /// Method to change GameObject's velocity based on its current acceleration.<br>
    /// Object will translate depending on how much velocity it carries, zeroing out the current acceleration.
    pub fn go(&mut self, delta_time: &usize) { //GameObject's PlayerMove trait equivalent
        //let decel_vec = self.velocity.mul_scalar((1.0/DECEL_FACTOR) * *delta_time as f64);
        //TODO: Look into this implementation of deceleration. Not convinced
        //it is the best way to do this.
        self.accel.add_mut(GRAVITY_VECTOR);
        if delta_time >= &100 {
            self.accel.sub_mut(self.velocity); //Decelerate
        } else {
            self.accel.sub_mut(self.velocity.mul(DECEL_VECTOR.mul_scalar(*delta_time as f64))); //Decelerate
        }
        self.velocity.add_mut(self.accel);
        let vel_mag = self.velocity.magnitude();
        if vel_mag < MINIMUM_VELOCITY_MAG {
            self.velocity = Vector3::ZERO;
        }
        self.translate(self.velocity.x, self.velocity.y, self.velocity.z);
        self.accel = Vector3::ZERO;

        //TODO: Remove conditional. (Used to not fall through ground)
        if self.pos.y < 0.0 + self.size / 2.0 {
            self.pos.y = 0.0 + self.size / 2.0
        }

        //TODO: Put rotation calculations in function?
        let axis = Vector3::new(0.0, 1.0, 0.0);
        let v = self.velocity;

        //TODO: Look into this rotation math. (works but feels hacky)
        // Apply rotation based on direction of movement
        let theta_tan = (v.x / v.z).atan();
        let theta_tan = if theta_tan.is_nan() {0.0} else {theta_tan}; // Zero if NaN
        let mut rotation = theta_tan + std::f64::consts::PI;
        if v.z < 0.0 {
            rotation += std::f64::consts::PI;
        }
        self.rotation = Quaternion::from_vector(axis, rotation);
    }

    pub fn get_children(&self) -> Rc<Vec<Rc<RefCell<VolatileObject>>>> {
        Rc::clone(&self.children)
    }
}

#[wasm_bindgen]
impl GameObject {
    pub fn new(x: f64, y: f64, z: f64, size: f64, obj_type: GameObjectType) -> Self {
        GameObject {
            pos: Vector3::new(x, y, z),
            size,
            accel: Vector3::ZERO,
            velocity: Vector3::ZERO,
            rotation: Quaternion::ZERO,
            obj_type,
            children: Rc::new(Vec::new()),
            collision_map_pos: Vector3::ZERO,
        }
    }

    /// Set absolute position of GameObject `(x, y, z)`.
    pub fn set_pos(& mut self, x: f64, y: f64, z: f64) {
        self.pos.x = x;
        self.pos.y = y;
        self.pos.z = z;
    }

    /// Translate GameObject based on delta `(x, y, z)`.
    pub fn translate(& mut self, x: f64, y: f64, z: f64) {
        self.pos.x += x;
        self.pos.y += y;
        self.pos.z += z;
    }
}

impl SHMDebug for GameObject {
    fn set_hash(&mut self, hash: &(i64, i64, i64)) {
        self.collision_map_pos = Vector3::new(hash.0 as f64, hash.1 as f64, hash.2 as f64);
    }
}

impl Position for GameObject {
    fn position(&self) -> Vector3 {
        self.pos
    }
}

/// Used for basic collision detection until an actual implementation is in place.<br>
/// Objects within this distance are colliding.

impl Collidable<GameObject> for GameObject {
    fn collide(&self, other: &GameObject) -> bool {
        self.pos.distance(other.pos) < (self.size / 2.0 + other.size / 2.0)
    }

    fn handle_collision(& mut self, other: & mut GameObject) {
        //TODO: implement this
        other.accel.add_mut(self.velocity);
        self.velocity.negate_mut();
    }
}

/// After velocity reaches this minimum, velocity will be set to zero.<br>
/// Without a minimum velocity, floating point for velocity will keep approaching zero, but take a long time to reach zero.
const MINIMUM_VELOCITY_MAG: f64 = 0.015625; // 1/64
/// Arbitrary value to divide time delta by to calculate actual acceleration speed.
const MOVE_SPEED_FACTOR: f64 = 0.015625;

impl PlayerMove for GameObject {
    fn update(& mut self, key_map: &HashMap<String, HashMap<String, bool>>, delta_time: &usize, absolute_time: &usize, camera_pos: &CameraPosition) {
        let speed: f64 = *delta_time as f64 * MOVE_SPEED_FACTOR;
        //camera_xxx_speed used to find translation speed of game object
        //relative to the camera position (moving left and right should be
        //based on the direction of the camera)
        let mut camera_cos_speed = camera_pos.theta.cos() * speed;
        let mut camera_sin_speed = camera_pos.theta.sin() * speed;

        //TODO: Remove (debug information to console)
        if key_pressed(&key_map, "H") {
            console_log_1(&self);
        }
        //////
        
        if key_pressed(&key_map, "V") {
            camera_cos_speed *= 2.0;
            camera_sin_speed *= 2.0;
        }
        if key_pressed(&key_map, "W") {
            let accel_d = Vector3::new(-camera_sin_speed, 0.0, -camera_cos_speed);
            self.accel.add_mut(accel_d);
        }
        if key_pressed(&key_map, "S") {
            let accel_d = Vector3::new(camera_sin_speed, 0.0, camera_cos_speed);
            self.accel.add_mut(accel_d);
        }
        if key_pressed(&key_map, "A") {
            let accel_d = Vector3::new(-camera_cos_speed, 0.0, camera_sin_speed);
            self.accel.add_mut(accel_d);
        }
        if key_pressed(&key_map, "D") {
            let accel_d = Vector3::new(camera_cos_speed, 0.0, -camera_sin_speed);
            self.accel.add_mut(accel_d);
        }
        if key_pressed(&key_map, "R") {
            self.set_pos(0.0, 0.0, 0.0);
            self.accel = Vector3::ZERO;
            self.velocity = Vector3::ZERO;
        }
        if key_pressed(&key_map, "space") {
            self.shoot(*absolute_time);
            /*
            let accel_d = Vector3::new(0.0, speed, 0.0);
            self.accel.add_mut(accel_d);
            */
        }
        if key_pressed(&key_map, "shift") {
            let accel_d = Vector3::new(0.0, -speed, 0.0);
            self.accel.add_mut(accel_d);
        }

        // Have objects translate based on velocity and acceleration.
        self.go(&delta_time);
        for o in self.children.iter() {
            let o = o.borrow();
            let mut game_object = o.object.borrow_mut();
            if absolute_time - o.birth_time_millis > o.lifetime_millis {
                //TODO: Remove objects instead of flying off screen
                game_object.accel.add_mut(Vector3::new(0.0, 100.0, 0.0));
            }
            game_object.go(&delta_time);
        }
    }
}

const SHELL_LIFETIME_MILLIS: usize = 5 * 1000;

impl Shoot for GameObject {
    fn shoot(& mut self, absolute_time: usize) {
        //TODO: This needs to be redone
        //TODO: Lifetime should be performance now, not time delta
        let shell = VolatileObject{
            birth_time_millis: absolute_time,
            lifetime_millis: SHELL_LIFETIME_MILLIS,
            object: RefCell::new(GameObject::new(self.pos.x, self.pos.y, self.pos.z, 10.0, GameObjectType::NPC))
        };
        let pure_quat = Quaternion::pure_quaternion(Vector3::new(0.0, 0.0, -100.0));
        let unit_quat = Quaternion::unit_quaternion(self.rotation);
        let rotation_conjugate = Quaternion::conjugate(unit_quat);
        let shell_accel = unit_quat.cross(pure_quat).cross(rotation_conjugate).to_vector(); 
        shell.object.borrow_mut().rotation = self.rotation;

        shell.object.borrow_mut().accel.add_mut(shell_accel);
        Rc::get_mut(&mut self.children).unwrap().push(Rc::new(RefCell::new(shell)));
    }
}
