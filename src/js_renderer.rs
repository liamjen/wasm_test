use crate::utils::{Vector3, Quaternion};

use wasm_bindgen::prelude::*;

#[wasm_bindgen(module = "world")]
extern {
    type World;

    #[wasm_bindgen(constructor)]
    fn new(width: usize, height: usize, num_objects: usize) -> World;
    #[wasm_bindgen(method)]
    fn update(this: &World, camera_pos: JsValue);
    #[wasm_bindgen(method)]
    fn push_object(this: &World, size: f64);
    #[wasm_bindgen(method)]
    fn push_volatile_object(this: &World, id: usize, size: f64);
    #[wasm_bindgen(method)]
    fn set_volatile_object_position(this: &World, id: usize, x: f64, y: f64, z: f64);
    #[wasm_bindgen(method)]
    fn rotate_volatile_object(this: &World, id: usize, x: f64, y: f64, z: f64, w: f64);
    #[wasm_bindgen(method)]
    fn initialize_player(this: &World, size: f64);
    #[wasm_bindgen(method)]
    fn set_object_position(this: &World, index: usize, x: f64, y: f64, z: f64);
    #[wasm_bindgen(method)]
    fn set_player_position(this: &World, x: f64, y: f64, z: f64);
    #[wasm_bindgen(method)]
    fn set_player_text(this: &World, text: JsValue);
    #[wasm_bindgen(method)]
    fn set_object_text(this: &World, index: usize, text: JsValue);
    #[wasm_bindgen(method)]
    fn rotate_player(this: &World, x: f64, y: f64, z: f64, w: f64);
    #[wasm_bindgen(method)]
    fn rotate_object(this: &World, index: usize, x: f64, y: f64, z: f64, w: f64);
    #[wasm_bindgen(method)]
    fn debug_position_info(this: &World, object: JsValue) -> JsValue;
    
}

pub struct WebGLRenderer {
    world: World,
}

const WIDTH: usize = 500;
const HEIGHT: usize = WIDTH;

//TODO: Look into moving `get_mouse_state` to `input_state.rs`
//Since the mouse movement is obtained from the renderer's canvas it might not be possible
impl WebGLRenderer {
    /// Creates a WebGLRenderer wrapping 'world' node module.
    /// world node module imports THREE.js, and uses the three module
    /// to call WebGL functions.
    pub fn new(num_objects: usize) -> Self {
        WebGLRenderer {
            world: World::new(WIDTH, HEIGHT, num_objects),
        }
    }
    
    pub fn set_object_position(&self, index: usize, pos: Vector3) {
        self.world.set_object_position(index, pos.x, pos.y, pos.z);
    }

    pub fn push_object(&self, size: f64) {
        self.world.push_object(size);
    }    
    
    pub fn push_volatile_object(&self, id: usize, size: f64) {
        self.world.push_volatile_object(id, size);
    }

    pub fn set_volatile_object_position(&self, id: usize, pos: Vector3) {
        self.world.set_volatile_object_position(id, pos.x, pos.y, pos.z);
    }

    pub fn rotate_volatile_object(&self, id: usize, rotation: Quaternion) {
        self.world.rotate_volatile_object(id, rotation.x, rotation.y, rotation.z, rotation.w);
    }

    pub fn initialize_player(&self, size: f64) {
        self.world.initialize_player(size);
    }
    
    pub fn set_player_position(&self, pos: Vector3) {
        self.world.set_player_position(pos.x, pos.y, pos.z);
    }

    pub fn debug_position_info(&self, object: JsValue) -> JsValue {
        self.world.debug_position_info(object)
    }

    pub fn set_object_text(&self, index: usize, text: JsValue) {
        self.world.set_object_text(index, text);
    }
    
    pub fn set_player_text(&self, text: JsValue) {
        self.world.set_player_text(text);
    }

    pub fn rotate_player(&self, q: Quaternion) {
        self.world.rotate_player(q.x, q.y, q.z, q.w);
    }    
    
    pub fn rotate_object(&self, index: usize, rotation: Quaternion) {
        self.world.rotate_object(index, rotation.x, rotation.y, rotation.z, rotation.w);
    }

    /// Draws all GameObjects at their respective locations according to
    /// game_state.rs objects Vec.
    pub fn update(&self, camera_pos: JsValue) {
        self.world.update(camera_pos);
    }
}