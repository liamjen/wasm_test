use wasm_bindgen::prelude::*;

#[wasm_bindgen(module = "input-state")]
extern {
    type InputState;
    #[wasm_bindgen(constructor)]
    fn new() -> InputState;

    #[wasm_bindgen(method)]
    fn keyboard_state(this: &InputState) -> JsValue;

    #[wasm_bindgen(method)]
    fn mouse_state(this: &InputState) -> JsValue;
    
    #[wasm_bindgen(method)]
    fn reset_mouse_state(this: &InputState);

    #[wasm_bindgen(method)]
    fn delta_time(this: &InputState) -> JsValue;

    #[wasm_bindgen(method)]
    fn absolute_time(this: &InputState) -> JsValue;

    #[wasm_bindgen(method)]
    fn update(this: &InputState);
}

pub struct InputStateRust {
    input_state: InputState,
}

impl InputStateRust {
    /// Creates a new InputStateRust struct wrapping `input-state` node module.
    /// input-state node module exports InputState class which returns the current
    /// keys pressed on keyboard and mouse changes.
    pub fn new() -> Self {
        InputStateRust {
            input_state: InputState::new(),
        }
    }

    pub fn get_delta_time(&self) -> usize {
        let delta_time: f64 = self.input_state.delta_time().into_serde().unwrap();
        delta_time as usize
    }

    pub fn get_absolute_time(&self) -> usize {
        let absolute_time: f64 = self.input_state.absolute_time().into_serde().unwrap();
        absolute_time as usize
    }

    /// Returns keys being pressed on keyboard as JsValue object.
    /// 
    /// JavaScript object:
    /// ```
    /// Object = {
    ///     "A": {
    ///         "pressed": true,
    ///     },
    ///     "B": {
    ///         "pressed": true,
    ///     }, ...
    /// }
    /// ```
    pub fn get_keyboard_state(&self) -> JsValue {
        self.input_state.update();
        self.input_state.keyboard_state()
    }

    /// Returns mouse delta change, as well as scroll as JsValue Object
    /// JavaScript object:
    /// ```
    /// mouseState = {
    ///     x: 0,
    ///     y: 0,
    ///     scroll: 0
    /// }
    /// ```
    pub fn get_mouse_state(&self) -> JsValue {
        self.input_state.mouse_state()
    }

    /// Set mouse state values back to zero.
    /// Mouse movement JS listener only reports delta movement, so
    /// after the value is read it is zeroed to stop repeated inputs.
    pub fn reset_mouse_state(&self) {
        self.input_state.reset_mouse_state();
    }
}