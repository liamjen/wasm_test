use crate::utils::{KeyListener, MouseListener, key_pressed, console_log_1};

use wasm_bindgen::prelude::*;

use std::collections::HashMap;

use std::f64::consts::PI;
const TWO_PI: f64 = 2.0 * PI;

const UPPER_CAMERA_LIMIT: f64 = 0.03125; // <- 1/32
const MINIMUM_ZOOM:       f64 = 5.0;

/// Struct holding state of GameCamera (speed, angle)
#[wasm_bindgen]
#[derive(Serialize, Debug)]
pub struct CameraPosition {
    pub phi:    f64,
    pub theta:  f64,
    pub radius: f64,
    speed: f64,
}

impl CameraPosition {
    /// Defaults for GameCamera
    pub const DEFAULT_CAMERA_POS: Self = CameraPosition {
        phi: 0.9, 
        theta: PI * 1.5, 
        radius: 500.0,
        speed: 0.0005,
    };
}

impl MouseListener for CameraPosition {
    fn update_mouse(& mut self, mouse_movement: &HashMap<String, i32>, delta_time: &usize) {
        if let Some(x) = mouse_movement.get("x") {
            if *x != 0 {
                self.theta -= (*delta_time as i32 * x) as f64 * self.speed;
                if self.theta < 0.0 {
                    self.theta = TWO_PI;
                }            
                else if self.theta > TWO_PI {
                    self.theta = 0.0;
                }
            }
        }
        if let Some(y) = mouse_movement.get("y") {
            if *y != 0 {
                self.phi -= (*delta_time as i32 * y) as f64 * self.speed;
                if self.phi < UPPER_CAMERA_LIMIT {
                    self.phi = UPPER_CAMERA_LIMIT;
                }
                else if self.phi > PI {
                    self.phi = PI;
                }
            }
        }
        if let Some(scroll) = mouse_movement.get("scroll") {
            if *scroll != 0 {
                self.radius += *scroll as f64;
                if self.radius < MINIMUM_ZOOM {
                    self.radius = MINIMUM_ZOOM;
                }
            }
        }
    }
}

impl KeyListener for CameraPosition {
    fn update_keys(& mut self, key_map: &HashMap<String, HashMap<String, bool>>, delta_time: &usize) {
        if key_pressed(&key_map, "up") {  
            self.phi += self.speed * *delta_time as f64;
            if self.phi > PI {
                self.phi = PI;
            }   
        }
        if key_pressed(&key_map, "down") {
            self.phi -= self.speed * *delta_time as f64;
            if self.phi < UPPER_CAMERA_LIMIT {
                self.phi = UPPER_CAMERA_LIMIT;
            } 
        }
        if key_pressed(&key_map, "left") {
            self.theta += self.speed * *delta_time as f64;
            if self.theta > TWO_PI {
                self.theta = 0.0;
            }
        }
        if key_pressed(&key_map, "right") {
            self.theta -= self.speed * *delta_time as f64;
            if self.theta < 0.0 {
                self.theta = TWO_PI;
            }
        }
    }
}
