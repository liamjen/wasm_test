use wasm_bindgen::prelude::*;

use crate::game_camera::CameraPosition;

use std::collections::HashMap;
use std::collections::hash_map;
use std::vec;
use std::iter::Iterator;
use std::iter::IntoIterator;
use std::rc::Rc;
use std::cell::RefCell;
use std::marker::PhantomData;

pub fn set_panic_hook() {
    // When the `console_error_panic_hook` feature is enabled, we can call the
    // `set_panic_hook` function at least once during initialization, and then
    // we will get better error messages if our code ever panics.
    //
    // For more details see
    // https://github.com/rustwasm/console_error_panic_hook#readme
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();
}

/// Returns a bool if key is pressed based on key_map.
/// # Arguments
/// 
/// * `key_map` - HashMap<String, HashMap<String, bool>> map of key string 'A', 'B', etc
/// to map of string -> bool ("pressed", "key_up", etc) -> true/false.
/// 
/// * `key` - Key to check if pressed.
pub fn key_pressed(key_map: &HashMap<String, HashMap<String, bool>>, key: &str) -> bool {
    if let Some(k) = key_map.get(key) {
        k["pressed"]
    } else {
        false
    }
}

pub use std::fmt::Debug;
use web_sys::console;

/// Helper function to print 1 item to JavaScript console.<br>
/// To print more than 1 item use the `format!()` macro.<br>
/// Marked for removal.
// TODO: Remove this?
pub fn console_log_1<T>(obj: T) where T: Debug{
    console::log_1(&format!("{:?}", obj).into());
}

/// Trait meant to be inherited by GameObject to determine and handle collisions.
pub trait Collidable<T> where T: Position {
    fn collide(&self, other: &T) -> bool;
    fn handle_collision(& mut self, other: & mut T);
}

/// Iterator whos state holds objects of collisions to check.
pub struct CollisionsIter<T, I> where T: Position + Collidable<T> + Debug,
                                      I: Iterator<Item=((i64, i64, i64), Vec<Rc<RefCell<T>>>)>{
    phantom: PhantomData<T>,
    cell_iter: I,
    obj_list: Vec<Rc<RefCell<T>>>,
    obj_list_i: usize,
    obj_list_j: usize,
}

impl <T, I>CollisionsIter<T, I> where T: Position + Collidable<T> + Debug,
                                      I: Iterator<Item=((i64, i64, i64), Vec<Rc<RefCell<T>>>)> {
    pub fn new(cell_iter: I) -> Self {
        //TODO: Look into Phantom and T
        CollisionsIter {phantom: PhantomData, 
        cell_iter,
        obj_list: vec!(),
        obj_list_i: 0,
        obj_list_j: 1,
        }
    }
}

/// Returned by `SpatialHashMap`. Iterator of Tuples where `Tuple.0` and `Tuple.1` collide.
impl <T, I> Iterator for CollisionsIter<T, I> where T: Position + Collidable<T> + Debug, 
                                                    I: Iterator<Item=((i64, i64, i64), Vec<Rc<RefCell<T>>>)>{
    type Item = (Rc<RefCell<T>>, Rc<RefCell<T>>);
    fn next(&mut self) -> Option<Self::Item>{
        loop {
            while let Some(obj) = self.obj_list.get(self.obj_list_i){
                while let Some(obj2) = self.obj_list.get(self.obj_list_j) {
                    self.obj_list_j += 1;
                    if obj.borrow().collide(&obj2.borrow()) {
                        return Some((Rc::clone(&obj), Rc::clone(&obj2)))
                    }
                }
                self.obj_list_i += 1;
                self.obj_list_j = self.obj_list_i + 1;
            }
            if let Some(obj_list) = self.cell_iter.next() {
                self.obj_list = obj_list.1;
            } else {
                return None
            }
        }
    }
}

/// Data Structure to hash objects based on their position. Objects with the same hash
/// (or in the same cell) will be checked against each other for collisions.
#[derive(Debug)]
pub struct SpatialHashMap<T> {
    cell_size: usize,
    pub contents: HashMap<(i64, i64, i64), Vec<Rc<T>>>,
}

/// Trait to set an object's field based on its hash in a SpatialHashMap.<br>
/// Marked for removal as it is only used for debugging purposes.
// TODO: Remove this trait
pub trait SHMDebug {
    fn set_hash(& mut self, hash: &(i64, i64, i64));
}

impl <T> SpatialHashMap<T> where T: Position + Collidable<T> + Debug + SHMDebug{
    pub fn insert_all(cell_size: usize, player: Rc<RefCell<T>>, objects: Rc<Vec<Rc<RefCell<T>>>>) ->  CollisionsIter<T, hash_map::IntoIter<(i64, i64, i64), Vec<Rc<RefCell<T>>>>>{
        let mut contents: HashMap<(i64, i64, i64), Vec<Rc<RefCell<T>>>> = HashMap::new();

        Self::hash_and_insert(player, cell_size, &mut contents);

        for obj in objects.iter() {
            Self::hash_and_insert(Rc::clone(obj), cell_size, &mut contents);
        }
        CollisionsIter::new(contents.into_iter())
    }

    fn hash_and_insert(obj: Rc<RefCell<T>>, cell_size: usize, contents: &mut HashMap<(i64, i64, i64), Vec<Rc<RefCell<T>>>>) {
        let hash = Self::hash(obj.borrow().position(), cell_size);
        //TODO: remove [debugging information]
        //#[cfg(feature="debug")]
        obj.borrow_mut().set_hash(&hash);
        ////
        if let Some(obj_list) = contents.get_mut(&hash) {
            obj_list.push(Rc::clone(&obj));
        } else {
            let mut v: Vec<Rc<RefCell<T>>> = Vec::new();
            v.push(Rc::clone(&obj));
            contents.insert(hash, v);
        }
    }

    //TODO: objects should have multiple hashes if they fall
    //in between cells, or close enough to clip through objects
    //close to the margins
    fn hash(obj_pos: Vector3, cell_size: usize) -> (i64, i64, i64) where T: Position{
        ((obj_pos.x / cell_size as f64) as i64,
         (obj_pos.y / cell_size as f64) as i64,
         (obj_pos.z / cell_size as f64) as i64,)
    }
}

/// Struct holding a 3D point (x, y, z).
#[wasm_bindgen]
#[derive(Copy, Clone, Serialize, Debug)]
pub struct Vector3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Vector3 {
    pub fn new(x: f64, y: f64, z: f64) -> Self {
        Vector3 { x, y, z }
    }

    /// Returns vector with all zero components (0, 0, 0).
    pub const ZERO: Self = Vector3 {x: 0.0, y: 0.0, z: 0.0};

    /// Returns vector with magnitude 1.
    /// 
    /// # Arguments
    /// 
    /// * `v` - Vector3 to create unit vector from.
    /// ```rust
    /// let v: Vector3 = Vector3::new(0.0, 0.0, 5.0);
    /// assert_eq!(Vector3::new(0.0, 0.0, 1.0), Vector3::unit_vector(v));
    /// ```
    pub fn unit_vector(v: Vector3) -> Self {
        let mag: f64 = (v.x.powf(2.0) + v.y.powf(2.0) + v.z.powf(2.0)).sqrt();

        Vector3 {
            x: v.x / mag,
            y: v.y / mag,
            z: v.z / mag
        }
    }

    /// Returns magnitude (length from 0, 0, 0 to vector coordinates) of vector.
    pub fn magnitude(&self) -> f64 {
        (self.x.powf(2.0) + self.y.powf(2.0) + self.z.powf(2.0)).sqrt()
    }

    /// Returns distance between two vector coordinates (magnitude of vector subtracted from another vector).
    pub fn distance(&self, other: Vector3) -> f64 {
        self.sub(other).magnitude()
    }

    /// Returns vector that is the addition of self and another vector.
    pub fn add(&self, other: Vector3) -> Self {
        Vector3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }

    /// Mutates self vector by adding all components of another vector.
    pub fn add_mut(& mut self, other: Vector3) {
        self.x += other.x;
        self.y += other.y;
        self.z += other.z;
    }

    /// Returns vector that is the subtraction of self and another vector.
    pub fn sub(&self, other: Vector3) -> Self {
        Vector3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }

    /// Mutates self vector by subtracting all components of another vector.
    pub fn sub_mut(& mut self, other: Vector3) {
        self.x -= other.x;
        self.y -= other.y;
        self.z -= other.z;
    }

    /// Returns vector that is the multiplication of self and another vector.
    pub fn mul(&self, other: Vector3) -> Self {
        Vector3 {
            x: self.x * other.x,
            y: self.y * other.y,
            z: self.z * other.z
        }
    }

    /// Mutates self vector by multiplying all components of another vector.
    pub fn mul_mut(&mut self, other: Vector3) {
        self.x *= other.x;
        self.y *= other.y;
        self.z *= other.z;
    }

    /// Returns vector that is the multiplication of self and a scalar value.
    pub fn mul_scalar(&self, scalar: f64) -> Self {
        Vector3 {
            x: self.x * scalar,
            y: self.y * scalar,
            z: self.z * scalar
        }
    }

    /// Mutates self vector by multiplying all components by a scalar value.
    pub fn mul_scalar_mut(&mut self, scalar: f64) {
        self.x *= scalar;
        self.y *= scalar;
        self.z *= scalar;
    }

    /// Returns vector that is the negation of self.
    pub fn negate(&self) -> Self {
        Vector3 {
            x: -self.x,
            y: -self.y,
            z: -self.z
        }
    }

    /// Mutably negates self vector.
    pub fn negate_mut(& mut self) {
        self.x = -self.x;
        self.y = -self.y;
        self.z = -self.z;
    }

    /*
    pub fn lerp(&self, other: Vector3) {

    }
    */
}

/// Struct representing a quaternion (x, y, z, w components).
/// Used for rotations in 3D space.
#[wasm_bindgen]
#[derive(Copy, Clone, Serialize, Debug)]
pub struct Quaternion {
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub w: f64,
}

impl Quaternion {
    /// Quaternion with all components set to zero
    pub const ZERO: Self = Quaternion {x: 0.0, y: 0.0, z: 0.0, w: 0.0};

    /// Returns quaternion representing a rotation about an axis vector, and an angle to rotate about the axis.
    /// 
    /// # Arguments
    /// * 'axis' - Vector3 axis to  rotate about (right hand rule)
    /// * 'angle' - Angle in radians to rotate
    pub fn from_vector(axis: Vector3, angle: f64) -> Self{
        let sin_angle: f64 = (angle / 2.0).sin();
        let cos_angle: f64 = (angle / 2.0).cos();
        Quaternion {
            x: axis.x * sin_angle,
            y: axis.y * sin_angle,
            z: axis.z * sin_angle,
            w: cos_angle,
        }
    }

    /// Returns quaternion where x, y, z components are from a vector (w component set to zero).
    /// 
    /// # Arguments
    /// * 'v' - Vector3 to take x, y, z components from 
    pub fn pure_quaternion(v: Vector3) -> Self{
        Quaternion {
            x: v.x,
            y: v.y,
            z: v.z,
            w: 0.0,
        }
    }

    // Drops 'w' component
    /// Returns Vector3 from quaternion (drops 'w' component).
    pub fn to_vector(&self) -> Vector3{
        Vector3 {
            x: self.x,
            y: self.y,
            z: self.z
        }
    }

    /// Returns quaternion that is a cross product of self and another quaternion.
    pub fn cross(&self, o: Self) -> Self {
        let v = self;

        Quaternion {
            w: o.w * v.w - o.x * v.x - o.y * v.y - o.z * v.z,
            x: o.w * v.x + o.x * v.w - o.y * v.z + o.z * v.y,
            y: o.w * v.y + o.x * v.z + o.y * v.w - o.z * v.x,
            z: o.w * v.z - o.x * v.y + o.y * v.x + o.z * v.w,
        }
    }

    //TODO: Test this function
    /// Conjugate of quaternion (x, y, z negated)
    pub fn conjugate(q: Self) -> Self {
        Quaternion {
            x: -q.x,
            y: -q.y,
            z: -q.z,
            w:  q.w
        }
    }

    //TODO: Test this function
    /// Returns quaternion with magnitude 1.
    pub fn unit_quaternion(q: Self) -> Self {
        let mag: f64 = (q.x.powf(2.0) + q.y.powf(2.0) + q.z.powf(2.0) + q.w.powf(2.0)).sqrt();

        Quaternion {
            x: q.x / mag,
            y: q.y / mag,
            z: q.z / mag,
            w: q.w / mag
        }
    }
}

// TODO: Probably redo this trait
/// Trait for GameObjects who can fire a shell.
pub trait Shoot {
    fn shoot(& mut self, absolute_time: usize);
}

/// Trait to return Vector3 position.
pub trait Position {
    fn position(&self) -> Vector3;
}

/// Trait for the 'player' GameObject to handle movement.
pub trait PlayerMove {
    fn update(& mut self, 
              key_map: &HashMap<String, HashMap<String, bool>>, 
              delta_time: &usize,
              absolute_time: &usize,
              camera_pos: &CameraPosition);
}

/// Trait for objects who mutate based on key inputs.
pub trait KeyListener {
    fn update_keys(& mut self, 
                   key_map: &HashMap<String, HashMap<String, bool>>, 
                   delta_time: &usize);
}

/// Trait for objects who mutate based on mouse inputs.
pub trait MouseListener {
    fn update_mouse(& mut self,
                    mouse_movement: &HashMap<String, i32>,
                    delta_time: &usize);
}