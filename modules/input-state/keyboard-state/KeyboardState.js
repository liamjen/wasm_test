/**
 * @author Lee Stemkoski, Liam Jensen
 *
 * Usage: 
 * (1) create a global variable:
 *      var keyboard = new KeyboardState();
 * (2) during main loop:
 *       keyboard.update();
 * (3) check state of keys:
 *       keyboard.down("A")    -- true for one update cycle after key is pressed
 *       keyboard.pressed("A") -- true as long as key is being pressed
 *       keyboard.up("A")      -- true for one update cycle after key is released
 * 
 *  See KeyboardState.k object data below for names of keys whose state can be polled
 */

function onKeyUp(self)
{
	return (event) => {
		var key = self.keyName(event.keyCode);
		if ( self.status[key] )
			self.status[key].pressed = false;
	}
}

function onKeyDown(self)
{
	return (event) => {
		var key = self.keyName(event.keyCode);
		if ( !this.status[key] )
			self.status[key] = { down: true, pressed: true, up: false};
	}
}
 
// initialization
module.exports = class KeyboardState {
	constructor() {
		// bind keyEventsa
		document.addEventListener("keydown", onKeyDown(this), false);
		document.addEventListener("keyup",   onKeyUp(this),   false);	

		this.k = 
		{  
			8: "backspace",  9: "tab",       13: "enter",    16: "shift", 
			17: "ctrl",     18: "alt",       27: "esc",      32: "space",
			33: "pageup",   34: "pagedown",  35: "end",      36: "home",
			37: "left",     38: "up",        39: "right",    40: "down",
			45: "insert",   46: "delete",   186: ";",       187: "=",
			188: ",",      189: "-",        190: ".",       191: "/",
			219: "[",      220: "\\",       221: "]",       222: "'"
		}

		this.status = {}
	}
	
	
	keyStatus() {
		return this.status;
	}
	
	keyName( keyCode )
	{
		return ( this.k[keyCode] != null ) ? 
			this.k[keyCode] : 
			String.fromCharCode(keyCode);
	}
	
	update()
	{
		for (var key in this.status)
		{
			// key has been flagged as "up" since last update
			if ( this.status[key].up ) 
			{
				delete this.status[key];
				continue; // move on to next key
			}
			
			if ( !this.status[key].pressed ) // key released
				this.status[key].up = true;
		}
	}
	
	down(keyName)
	{
		return (this.status[keyName] && this.status[keyName].down);
	}
	
	pressed(keyName)
	{
		return (this.status[keyName] && this.status[keyName].pressed);
	}
	
	up(keyName)
	{
		return (this.status[keyName] && this.status[keyName].up);
	}
	
	debug()
	{
		var list = "Keys active: ";
		for (var arg in this.status)
			list += " " + arg
		console.log(list);
	}
	
	

}

///////////////////////////////////////////////////////////////////////////////

