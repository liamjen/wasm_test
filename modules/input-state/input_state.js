const KeyboardState = require('keyboard-state');

class InputState {
    constructor() {
        this.keyboard = new KeyboardState();
        this.frame_count = {
            last_time: 0,
            last_update: 0,
            frames: 0
        };

        this.mouseMovement = {
            x: 0,
            y: 0,
            scroll: 0
        };

        // create variable referring to `this` for use in event listening functions.
        var self = this;
        const update_mouse_pos = function(e) {
            if(document.pointerLockElement) {
                self.mouseMovement.x = e.movementX;
                self.mouseMovement.y = e.movementY;
            }
        }
        const update_mouse_wheel = function(e) {
            if(document.pointerLockElement) {
                self.mouseMovement.scroll = e.deltaY;
            }
        }
        document.addEventListener('mousemove', update_mouse_pos, {passive: false});
        window.addEventListener('wheel', update_mouse_wheel, {passive: true});
    }
    

    update() {
        this.keyboard.update();
    }

    // Returns absolute time in millis
    absolute_time() {
        return this.frame_count.last_time;
    }

    delta_time() {
        let now = performance.now();
        let delta_time = now - this.frame_count.last_time;
        this.frame_count.last_time = now;
        return delta_time;
    }

    keyboard_state() {
        return this.keyboard.keyStatus();
    }

    mouse_state() {
        return this.mouseMovement;
    }

    reset_mouse_state() {
        this.mouseMovement.x = 0;
        this.mouseMovement.y = 0;
        this.mouseMovement.scroll = 0;
    }
}

module.exports.InputState = InputState;
