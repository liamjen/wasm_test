//TODO: Completely refactor this (3D graphics <-> rust)

const THREE = require('three');
const GLTFLoader = require('three-gltf-loader');

// Map of string to loaded object (gltf)
const loaded_objects = {};

class World {
    constructor(width, height, num_objects) {
        this.width = width;
        this.height = height;

        // Initialize three.js objects
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(40, width / height, 0.1, 5000);
        this.renderer = new THREE.WebGLRenderer({antialias: false});
        this.renderer.setClearColor(0xffffff, 1);

        this.renderer.setSize(width, height);
        document.body.appendChild(this.renderer.domElement);
        
        this.player = undefined;
        // Holds all updatable NPC objects
        this.objects = [];

        this.volatile_objects = {};

        const player_load_function = () => {
            this.player.set_scale(50);
        }

        this.player = new LoadObject(this.scene, 'models/tank.glb', player_load_function);

        this.renderer.domElement.addEventListener('mousedown', function(e) {
            e.srcElement.requestPointerLock();
        });

        const fullscreen_change = function(e) {
            var width = window.innerWidth;
            var height = window.innerHeight;
            if(document.fullscreenElement) {
                width = screen.width;
                height = screen.height;
            }
            self.renderer.setSize(width, height);
            self.camera.aspect = width / height;
            self.camera.updateProjectionMatrix();
        }

        var self = this;
        document.addEventListener('keydown', function(e) {
            if(e.key == "f") {
                self.requestFullscreen(e);
            }
        });        
        this.renderer.domElement.addEventListener('fullscreenchange', fullscreen_change);

        this.floor = new Floor(this.scene);
        this.light = new Light(this.scene);
    }

    set_object_position(index, x, y, z) {
        this.objects[index].set_position(x, y, z);
    }

    debug_position_info(obj) {
        let t = obj.obj_type.toUpperCase() + "\n" +
            "MAP:" + 
            obj.collision_map_pos.x + " " +
            obj.collision_map_pos.y + " " +
            obj.collision_map_pos.z + " "+ "\n" +
            "POS:" + 
            parseFloat(Number(obj.pos.x)).toFixed(0) + " " +
            parseFloat(Number(obj.pos.y)).toFixed(0) + " " +
            parseFloat(Number(obj.pos.z)).toFixed(0) + " ";
        return t;
    }

    set_object_text(index, text) {
        this.objects[index].text.set_text(text);
    }

    set_player_position(x, y, z) {
        this.player.set_position(x, y, z);
    }

    set_player_text(text) {
        this.player.text.set_text(text);
    }

    initialize_player(size) {
        this.player.set_scale(size);
    }

    push_object(size) {
        let b = new Box(this.scene, size);
        this.objects.push(b);
    }

    push_volatile_object(id, size) {
        if(this.volatile_objects[id] == undefined) {
            let obj = new LoadObject(this.scene, 'models/shell.glb');
            this.volatile_objects[id] = obj;
        }
    }

    set_volatile_object_position(id, x, y, z) {
        if(this.volatile_objects[id] != undefined) {
            this.volatile_objects[id].set_position(x, y, z);
        }
    }

    rotate_volatile_object(id, x, y, z, w) {
        if(this.volatile_objects[id] != undefined) {
            this.volatile_objects[id].set_rotation(x, y, z, w);
        }
    }

    remove_volatile_object(id) {
        this.volatile_objects.remove(id);
    }

    requestFullscreen(event) {
        this.renderer.domElement.requestFullscreen().then(() => console.log('fullscreen'),
                                                          () => console.log('failed'));
    }

    resize() {
        var width = window.innerWidth / 1.5;
        var height = window.innerHeight / 1.5;
        console.log(document.fullscreenElement)
        if(document.fullscreenElement) {
            console.log('fullscreen');
            width = screen.width;
            height = screen.height;
        }
        this.renderer.setSize(width, height);
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
    }

    rotate_player(x, y, z, w) {
        if(!this.player.load_object) {
            return;
        } else {
            var quaternion = new THREE.Quaternion(x, y, z, w);
            this.player.load_object.setRotationFromQuaternion(quaternion);
        }
    }

    rotate_object(i, x, y, z, w) {
        var quaternion = new THREE.Quaternion(x, y, z, w);
        this.objects[i].mesh.setRotationFromQuaternion(quaternion);
    }

    update(camera_pos) {
        if(!this.player.load_object) {
            return;
        }
        if(this.load_object && this.load_object_anim) {
            this.clips.forEach((e) => {
                this.load_object_anim.clipAction(e).play();
            })
        }
        
        this.camera.position.setFromSpherical(new THREE.Spherical(camera_pos.radius, camera_pos.phi, camera_pos.theta)).add(this.player.load_object.position);
        this.camera.lookAt(this.player.load_object.position);    
        this.light.light.position.set(this.player.load_object.position).addScalar(10);
        this.light.light.updateMatrix();
        this.light.light.updateMatrixWorld();
        this.light.pointLightHelper.update();

        this.renderer.render(this.scene, this.camera);
    }
}

class LoadObject {
    // onload_function called after gltf object is loaded.
    constructor(scene, gltf_path, onload_function) {
        this.scene = scene;
        this.gltf_path = gltf_path;

        if(loaded_objects[gltf_path] != undefined) {
            this.load_object = loaded_objects[gltf_path].scene.clone();
            this.scene.add(this.load_object);
        } else {
            var loader = new GLTFLoader();
            this.load_object = null;
            this.load_object_anim = null;
    
            loader.load(gltf_path, (gltf) => {
                gltf.scene.position.set(0, 10, 10);
                this.load_object = gltf.scene;
                this.load_object.scale.set(10, 10, 10);
                this.scene.add(gltf.scene);
                loaded_objects[gltf_path] = gltf;
                if(onload_function)
                    onload_function();
                //this.clips = gltf.animations;
                //this.load_object_anim = new THREE.AnimationMixer(gltf.scene);
            });
        }

        this.text = new TextSprite(scene, "");
    }

    set_position(x, y, z) {
        if(!this.load_object) {
            return;
        }
        this.load_object.position.set(x, y, z);
        this.text.set_position(x, y + 10, z);
    }

    set_scale(scalar) {
        if(!this.load_object) {
            return
        } else {
            this.load_object.scale.set(scalar, scalar, scalar);
        }
    }

    set_rotation(x, y, z, w) {
        if(!this.load_object) {
            return
        } else {
            let q = new THREE.Quaternion(x, y, z, w);
            this.load_object.setRotationFromQuaternion(q);
        }
    }
}

class Light {
    constructor(scene) {
        this.light = new THREE.PointLight(0x404040, 1.0, 4000);
        this.light.position.set(50, 100, 0);
        this.pointLightHelper = new THREE.PointLightHelper(this.light, 1);
        this.light2 = new THREE.PointLight(0x404040, 3.0, 4000);
        this.light2.position.set(0, 50, 50);
        scene.add(this.light, this.light2, this.pointLightHelper);
    }
}

class Box {
    constructor(scene, size) {
        this.geometry = new THREE.BoxGeometry( size, size, size );
        this.material = new THREE.MeshPhongMaterial( { color: 0x00ff00 } );
        this.material.flatShading = true;
        this.mesh = new THREE.Mesh(this.geometry, this.material);
        this.mesh.position.set(0, 10, 0);
        //this.text = new Text3D(scene, "");
        this.text = new TextSprite(scene, "");
        scene.add(this.mesh);
    }

    set_position(x, y, z) {
        this.mesh.position.set(x, y, z);
        this.text.set_position(x, y + 10, z);
    }
}

let starTexture = new THREE.TextureLoader().load('images/star.png');
let starMaterial = new THREE.SpriteMaterial({
    map: starTexture,
});

class StarSprite {
    constructor(scene, message) {
        //TODO: Refactor class
        this.scene = scene;

        this.sprite = new THREE.Sprite(starMaterial);
        this.sprite.center.set(0, 1);
        scene.add(this.sprite);
    }

    set_position(x, y, z) {
        this.sprite.position.set(x, y, z);
    }
}

class Sphere {
    constructor(scene, size) {
        this.scene = scene;
        this.geometry = new THREE.SphereGeometry(size / 2);
        this.material = new THREE.MeshToonMaterial({color: 0xff00ff});
        this.material.flatShading = true;
        this.mesh = new THREE.Mesh(this.geometry, this.material);
        this.mesh.position.set(0, 5, 0);
        //this.text = new Text3D(scene, "");
        this.text = new TextSprite(scene, "");

        scene.add(this.mesh);
    }

    set_position(x, y, z) {
        this.mesh.position.set(x, y, z);
        //let s = new StarSprite(this.scene);
        //s.set_position(x, y, z);
        this.text.set_position(x, y + 20, z);
    }
}

class TextSprite {
    constructor(scene, message) {
        //TODO: Refactor class
        this.message = message;
        this.scene = scene;

        this.parameters = {
            fontface: "Arial",
            fontsize: 10,
        }

        this.canvas = document.createElement('canvas');
        this.context = this.canvas.getContext('2d');
        this.context.font = this.parameters.fontsize + "px sans-serif";
        this.context.fillStyle = "rgba(0, 0, 0, 1.0)"
        this.context.textAlign = "left"
        this.sprite = new THREE.Sprite();
        this.sprite.center.set(0, 1);
        scene.add(this.sprite);
        this.update_context(message);
    }

    update_context(message) {
        if(this.message == message) {
            return
        } else {
            this.message = message;
        }
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        let message_lines = message.split("\n");
        let x = 0, y = 0;
        let line_height = this.parameters.fontsize;
        
        var width = 0;
        let height = line_height * message_lines.length;
        message_lines.forEach((e) => {
            let w = this.context.measureText(e).width;
            width = w > width ? w : width;
        });

        this.canvas.width = Math.max(2, THREE.Math.ceilPowerOfTwo(width));
        this.canvas.height = Math.max(2, THREE.Math.ceilPowerOfTwo(height));
        this.context.strokeRect(0, 0, width, this.canvas.height);
        
        for(var i = 0; i < message_lines.length; i++) {
            this.context.strokeText(message_lines[i], x, y + ((i+1) * line_height));
        }

        var texture = new THREE.CanvasTexture(this.canvas);
        let spriteMaterial = new THREE.SpriteMaterial({
            map: texture,
        });
        this.sprite.material = spriteMaterial;
        this.sprite.scale.set(this.canvas.width / 4, this.canvas.height / 4, 1.0);
    }

    set_text(message) {
        this.update_context(message);
    }

    set_position(x, y, z) {
        this.sprite.position.set(x, y, z);
    }
}

class Text3D {
    constructor(scene, text) {
        this.scene = scene;
        this.text = text;
        var loader = new THREE.FontLoader();
        this.material = new THREE.MeshPhongMaterial( { color: 0x0000ff } );
        loader.load('font/mono.json', (font) => {
            this.fontParams = {
                font: font,
                size: 2,
                height: 0.5,
            }
            this.textGeo = new THREE.TextGeometry(text, this.fontParams);
            this.mesh = new THREE.Mesh(this.textGeo, this.material);
            this.scene.add(this.mesh);
        });
    }

    set_text(text) {
        if(this.fontParams && this.text != text) {
            let pos = this.mesh.position;
            this.text = text;
            this.scene.remove(this.mesh);
            this.textGeo = new THREE.TextGeometry(text, this.fontParams);
            this.mesh = new THREE.Mesh(this.textGeo, this.material);
            this.mesh.position.set(pos)
            this.scene.add(this.mesh);
        }
    }

    set_position(x, y, z) {
        if(this.mesh) {
            this.mesh.position.set(x, y, z);
        }
    }
}

class Floor {
    constructor(scene) {
        this.floor = new THREE.PlaneGeometry(5000, 5000, 10, 10);
        this.floorTexture = new THREE.TextureLoader().load('images/floor.jpg');
        this.floorTexture.wrapS = this.floorTexture.wrapT = THREE.RepeatWrapping;
        this.floorTexture.repeat.set(10, 10);
        this.floor_mat = new THREE.MeshBasicMaterial({map: this.floorTexture, side: THREE.DoubleSide});
        this.mesh = new THREE.Mesh(this.floor, this.floor_mat);
        this.mesh.rotation.x = Math.PI / 2;
        scene.add(this.mesh);
    }
}

module.exports.World = World;
