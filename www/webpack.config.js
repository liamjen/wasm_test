const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require('path');

module.exports = {
  entry: "./bootstrap.js",
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "bootstrap.js",
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    compress:true,
    port: 8080,
  },
  resolve: {
    modules: ['./node_modules', '../modules'],
  },
  mode: "development",
  plugins: [
    new CopyWebpackPlugin(['index.html'])
  ],
};
